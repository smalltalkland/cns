use std::net::*;
use std::io::prelude::*;
pub fn write_header(stream: &mut TcpStream) -> std::io::Result<Result<(),()>>{
    let a = [54,24,34,84];
    stream.write(&a)?;
    let mut b = [0; 4];
    stream.read(&mut b)?;
    if b != a{return Ok(Err(()))};
Ok(Ok(()))
}
pub fn get_ip<'a>(target: &'a mut TcpStream) -> std::io::Result<TcpStream>{
let mut buf = String::with_capacity(256);
target.read_to_string(&mut buf)?;
Ok(TcpStream::connect(buf)?)
}
pub fn connect<'a>(address: &[&str],root: &'a mut TcpStream) -> std::io::Result<&'a mut TcpStream>{
    let mut target = root;
    for it in address{
        match write_header(target)?{
            Ok(_) => {},
            Err(_) => return Ok(target)
        };
        target.write(&[1])?;
        write!(target,"{}",it)?;
        target.write(&[0])?;
        *target = get_ip(target)?;
    };
    Ok(target)
}